<?xml version="1.0"?>
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:local="http://xsl/local" exclude-result-prefixes="xs"
	xmlns:tcx="http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2">
	<xsl:output indent="yes" />
	<xsl:param name="input" />

	<xsl:template match="/" name="main">
		<xsl:choose>
			<xsl:when test="unparsed-text-available($input)">
				<xsl:variable name="csv" select="unparsed-text($input)" />
				<xsl:variable name="lines" as="item()*">
					<xsl:for-each select="local:tokenize($csv, '\n')">
						<xsl:if test=". != ''">
							<xsl:value-of select="normalize-space(translate(.,'&quot;',''))" />
						</xsl:if>
					</xsl:for-each>
				</xsl:variable>
				<xsl:variable name="header">
					<xsl:for-each select="tokenize($lines[1],',')">
						<xsl:element name="value"
							namespace="http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2">
							<xsl:attribute name="name"><xsl:value-of
								select="." /></xsl:attribute>
						</xsl:element>
					</xsl:for-each>
				</xsl:variable>

				<xsl:variable name="data">
					<xsl:for-each select="$lines[position() != 1]">
						<xsl:element name="line"
							namespace="http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2">
							<xsl:for-each select="tokenize(.,',')">
								<xsl:variable name="index" select="position()" />
								<xsl:element name="value"
									namespace="http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2">
									<xsl:attribute name="name"><xsl:value-of
										select="$header/tcx:value[$index]/@name" /></xsl:attribute>
									<xsl:value-of select="." />
								</xsl:element>
							</xsl:for-each>
						</xsl:element>
					</xsl:for-each>
				</xsl:variable>

				<xsl:variable name="size" select="number(count($data/tcx:line))" />
				<xsl:variable name="firstline" select="$data/tcx:line[1]" />
				<xsl:variable name="lastline" select="$data/tcx:line[$size]" />
				<xsl:variable name="avgHR"
					select="round(avg(for $v in $data/tcx:line/tcx:value[@name = 'HEARTRATE'] return number($v)))" />
				<xsl:variable name="maxHR"
					select="max(for $v in $data/tcx:line/tcx:value[@name = 'HEARTRATE'] return number($v))" />
				<xsl:variable name="maxSpeed"
					select="max(for $v in $data/tcx:line/tcx:value[@name = 'SPEED'] return number($v))" />
				<xsl:variable name="distance"
					select="$lastline/tcx:value[@name = 'DISTANCE']" />
				<xsl:variable name="calories"
					select="round(number($lastline/tcx:value[@name = 'CALORIEBURN']))" />

				<xsl:variable name="start"
					select="xs:long($firstline/tcx:value[@name = 'timestamp_epoch'])" />
				<xsl:variable name="end"
					select="xs:long($lastline/tcx:value[@name = 'timestamp_epoch'])" />
				<xsl:variable name="total" select="xs:long(($end - $start) div 1000)" />
				<xsl:variable name="startTime" select="local:isodate($start)" />

				<TrainingCenterDatabase
					xmlns="http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2"
					xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
					xsi:schemaLocation="http://www.garmin.com/xmlschemas/ActivityExtension/v2 http://www.garmin.com/xmlschemas/ActivityExtensionv2.xsd http://www.garmin.com/xmlschemas/FatCalories/v1 http://www.garmin.com/xmlschemas/fatcalorieextensionv1.xsd http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2 http://www.garmin.com/xmlschemas/TrainingCenterDatabasev2.xsd">
					<Folders/>
					<Activities>
						<Activity Sport="Biking">
							<Id>
								<xsl:value-of select="local:isodate($start)" />
							</Id>
							<Lap StartTime="{$startTime}">
								<TotalTimeSeconds>
									<xsl:value-of select="$total" />
								</TotalTimeSeconds>
								<DistanceMeters>
									<xsl:value-of select="$distance" />
								</DistanceMeters>
								<MaximumSpeed>
									<xsl:value-of select="$maxSpeed" />
								</MaximumSpeed>
								<Calories>
									<xsl:value-of select="$calories" />
								</Calories>
								<AverageHeartRateBpm>
									<Value>
										<xsl:value-of select="$avgHR" />
									</Value>
								</AverageHeartRateBpm>
								<MaximumHeartRateBpm>
									<Value>
										<xsl:value-of select="$maxHR" />
									</Value>
								</MaximumHeartRateBpm>
								<Intensity>Active</Intensity>
								<TriggerMethod>Distance</TriggerMethod>
								<Track>
									<xsl:for-each select="$data/tcx:line">
										<Trackpoint>
											<xsl:call-template name="make_trackpoint">
												<xsl:with-param name="line" select="." />
											</xsl:call-template>
										</Trackpoint>
									</xsl:for-each>
								</Track>
							</Lap>
							<Creator xsi:type="Device_t">
								<Name>melastmohican</Name>
								<UnitId>7</UnitId>
								<ProductID>7</ProductID>
								<Version>
									<VersionMajor>1</VersionMajor>
									<VersionMinor>0</VersionMinor>
									<BuildMajor>1</BuildMajor>
									<BuildMinor>0</BuildMinor>
								</Version>
							</Creator>
						</Activity>
					</Activities>
					<Workouts />
					<Courses />
					<Author xsi:type="Application_t">
						<Name>Garmin Training Center</Name>
						<Build>
							<Version>
								<VersionMajor>1</VersionMajor>
								<VersionMinor>0</VersionMinor>
								<BuildMajor>1</BuildMajor>
								<BuildMinor>0</BuildMinor>
							</Version>
							<Type>Release</Type>
							<Time>Oct 3 2012, 17:00:00</Time>
							<Builder>melastmohican</Builder>
						</Build>
						<LangID>en</LangID>
						<PartNumber>006-A0183-00</PartNumber>
					</Author>
				</TrainingCenterDatabase>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>Cannot locate : </xsl:text>
				<xsl:value-of select="$input" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="make_trackpoint" match="Trackpoint">
		<xsl:param name="line" />
		<xsl:element name="Time"
			namespace="http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2">
			<xsl:variable name="tm"
				select="xs:long($line/tcx:value[@name = 'timestamp_epoch'])" />
			<xsl:value-of
				select="xs:dateTime('1970-01-01T00:00:00Z') + xs:long($line/tcx:value[@name = 'timestamp_epoch']) * xs:dayTimeDuration('PT0.001S')" />
		</xsl:element>
		<xsl:element name="Position"
			namespace="http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2">
			<xsl:element name="LatitudeDegrees"
				namespace="http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2">
				<xsl:value-of select="$line/tcx:value[@name = 'LATITUDE']" />
			</xsl:element>
			<xsl:element name="LongitudeDegrees"
				namespace="http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2">
				<xsl:value-of select="$line/tcx:value[@name = 'LONGITUDE']" />
			</xsl:element>
		</xsl:element>
		<xsl:element name="AltitudeMeters"
			namespace="http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2">
			<xsl:value-of select="$line/tcx:value[@name = 'ELEVATION']" />
		</xsl:element>
		<xsl:element name="DistanceMeters"
			namespace="http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2">
			<xsl:value-of select="$line/tcx:value[@name = 'DISTANCE']" />
		</xsl:element>
		<xsl:element name="HeartRateBpm"
			namespace="http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2">
			<xsl:element name="Value"
				namespace="http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2">
				<xsl:value-of select="round(number($line/tcx:value[@name = 'HEARTRATE']))" />
			</xsl:element>
		</xsl:element>
		<xsl:element name="Cadence"
			namespace="http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2">
			<xsl:value-of select="$line/tcx:value[@name = 'CADENCE']" />
		</xsl:element>
		<xsl:element name="SensorState"
			namespace="http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2">
			<xsl:value-of select="'Absent'" />
		</xsl:element>
	</xsl:template>

	<xsl:function name="local:tokenize" as="item()*">
		<xsl:param name="line" as="xs:string" />
		<xsl:param name="sep" as="xs:string" />
		<xsl:sequence select="(tokenize($line,$sep))" />
	</xsl:function>

	<xsl:function name="local:isodate">
		<xsl:param name="epoch" as="xs:long" />
		<xsl:value-of
			select="xs:dateTime('1970-01-01T00:00:00Z') + xs:long($epoch) * xs:dayTimeDuration('PT0.001S')" />
	</xsl:function>


</xsl:stylesheet>