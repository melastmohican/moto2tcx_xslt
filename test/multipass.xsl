<xsl:stylesheet version="1.0"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:ext="http://exslt.org/common">
	<xsl:output omit-xml-declaration="yes" indent="yes"/>
	<xsl:strip-space elements="*"/>

	<xsl:template match="node()|@*">
		<xsl:copy>
			<xsl:apply-templates select="node()|@*"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="node()|@*" mode="mPass2">
		<xsl:copy>
			<xsl:apply-templates select="node()|@*" mode="mPass2"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="/">
		<xsl:variable name="vrtfPass1Result">
			<xsl:apply-templates/>
		</xsl:variable>

		<xsl:apply-templates mode="mPass2"
      select="ext:node-set($vrtfPass1Result)/*"/>
	</xsl:template>

	<xsl:template match="num/text()">
		<xsl:value-of select="2*."/>
	</xsl:template>

	<xsl:template match="/*" mode="mPass2">
		<xsl:copy>
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates mode="mPass2"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="num/text()" mode="mPass2">
		<xsl:value-of select="3 + ."/>
	</xsl:template>
</xsl:stylesheet>