<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:date="http://exslt.org/dates-and-times"
                extension-element-prefixes="date">

    <xsl:template match="node">
        <xsl:value-of select="date:add('1970-01-01T00:00:00Z', date:duration(@stamp div 1000))"/>
    </xsl:template>

</xsl:stylesheet>