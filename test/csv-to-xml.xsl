<?xml version="1.0"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 	xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs">
	<xsl:output indent="yes"/>
	<xsl:param name="input"/>

	<xsl:template match="/" name="main">
		<xsl:choose>
			<xsl:when test="unparsed-text-available($input)">
				<xsl:variable name="csv" select="unparsed-text($input)"/>
				<xsl:variable name="lines" select="tokenize($csv, '\n')" as="xs:string+"/>
				<xsl:variable name="header" select="tokenize(normalize-space($lines[1]),',')" as="xs:string+"/>
				<csv>
					<xsl:for-each select="$lines[position() > 1]">
						<xsl:if test="normalize-space(.) != ''">
							<xsl:variable name="line" select="tokenize(normalize-space(.),',')" />
							<record>
								<xsl:for-each select="$header">
									<xsl:variable name="pos" select="position()"/>
									<field name="{replace(.,'&quot;','')}">
										<xsl:value-of select="replace($line[$pos],'&quot;','')"/>
									</field>
								</xsl:for-each>
							</record>
						</xsl:if>
					</xsl:for-each>
				</csv>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>Cannot locate : </xsl:text>
				<xsl:value-of select="$input"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>